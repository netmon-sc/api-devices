<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ApiServer\Authorization\Models\Role;
use ApiServer\Authorization\Models\Permission;

class CreateDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	//http://netjson.org/rfc.html#DeviceConfiguration
        Schema::create('devices', function (Blueprint $table) {
        	//meta
            $table->uuid('id')->primary();
            $table->uuid('owner_id')->nullable();
            $table->foreign('owner_id')
	            ->references('id')
    	        ->on('users')
        	    ->onDelete('cascade');

            $table->uuid('creator_id')->nullable();
            $table->foreign('creator_id')
	            ->references('id')
    	        ->on('users')
        	    ->onDelete('cascade');

            //general
            $table->string('hostname', 255);
            $table->text('description')->nullable();
            $table->float('latitude', 10, 6)->nullable();
            $table->float('longitude', 10, 6)->nullable();

/*
            //hardware
            $table->string('hardware_manufacturer', 200)->nullable();
            $table->string('hardware_model', 200)->nullable();
            $table->string('hardware_version', 200)->nullable();
            $table->string('hardware_cpu', 200)->nullable();

            //operating system
            $table->string('os_name', 200)->nullable();
            $table->string('os_kernel', 200)->nullable();
            $table->string('os_version', 200)->nullable();
            $table->string('os_revision', 200)->nullable();
            $table->text('os_description')->nullable();

            //resources
            $table->integer('resources_memory_total')->nullable();
            $table->integer('resources_swap_total')->nullable();
            $table->integer('resources_cpu_frequency')->nullable();
            $table->integer('resources_flash_total')->nullable();
            $table->integer('resources_storage_total')->nullable();*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('devices');
    }
}
