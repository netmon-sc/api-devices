e<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ApiServer\Authorization\Models\Role;
use ApiServer\Authorization\Models\Permission;

class CreateDeviceStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_statuses', function (Blueprint $table) {
        	//meta
            $table->uuid('id')->primary();
            $table->uuid('device_id');
            $table->foreign('device_id')
		            ->references('id')
        		    ->on('devices')
            		->onDelete('cascade');

            //general
			$table->integer('local_time')->nullable();
			$table->integer('uptime')->nullable();
			$table->integer('idletime')->nullable();
            $table->integer('clients')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_statuses');
    }
}
