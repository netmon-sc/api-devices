e<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ApiServer\Authorization\Models\Role;
use ApiServer\Authorization\Models\Permission;
use ApiServer\Configs\Models\Config;

class CreateDeviceAuthorizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create db table
        Schema::create('device_authorizations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('device_id');
            $table->foreign('device_id')
		            ->references('id')
        		    ->on('devices')
            		->onDelete('cascade');
            $table->string('password', 60);
            $table->string('type', 50);
            $table->timestamps();
        });

        //setup permissions
        $adminRoleId = Config::where(
            'key',
            'serverAdminRoleId'
        )->firstOrFail()->value;
        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'index',
            'resource_id' => 'device_authorization',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'show',
            'resource_id' => 'device_authorization',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'store',
            'resource_id' => 'device_authorization',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'update',
            'resource_id' => 'device_authorization',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'destroy',
            'resource_id' => 'device_authorization',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop permissions
        Permission::where('resource_id', '=', 'device_status')->delete();

        //drop table
        Schema::drop('device_authorizations');
    }
}
