<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveLatLonToCoordinates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->dropColumn([
                'latitude',
                'longitude',
            ]);
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->uuid('coordinate_id')->nullable()->after('creator_id');
            $table->foreign('coordinate_id')
	            ->references('id')
    	        ->on('coordinates')
        	    ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->float('latitude', 10, 6)->nullable();
            $table->float('longitude', 10, 6)->nullable();
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->dropForeign(['coordinate_id']);
            $table->dropColumn([
                'coordinate_id'
            ]);
        });
    }
}
