<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreDeviceStatusesAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_statuses', function (Blueprint $table) {
            //resources
			$table->float('resources_load_1min')->nullable()->after('clients');
			$table->float('resources_load_5min')->nullable()->after('resources_load_1min');
			$table->float('resources_load_15min')->nullable()->after('resources_load_5min');

			$table->integer('resources_memory_total')->nullable()->after('resources_load_15min');
			$table->integer('resources_memory_free')->nullable()->after('resources_memory_total');
			$table->integer('resources_memory_buffered')->nullable()->after('resources_memory_free');
			$table->integer('resources_memory_cache')->nullable()->after('resources_memory_buffered');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_statuses', function (Blueprint $table) {
            $table->dropColumn([
                'resources_load_1min',
                'resources_load_5min',
                'resources_load_15min',
                
                'resources_memory_total',
                'resources_memory_free',
                'resources_memory_buffered',
                'resources_memory_cache',
            ]);
        });
    }
}
