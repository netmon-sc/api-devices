<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ApiServer\Authorization\Models\Role;
use ApiServer\Authorization\Models\Permission;

class AddMoreDeviceAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devices', function (Blueprint $table) {
            // hardware
            $table->string('hardware_manufacturer')->nullable()->after('longitude');
            $table->string('hardware_model')->nullable()->after('hardware_manufacturer');
            $table->string('hardware_version')->nullable()->after('hardware_model');
            $table->string('hardware_cpu')->nullable()->after('hardware_version');

            // operating system
            $table->string('os_name')->nullable()->after('hardware_cpu');
            $table->string('os_kernel')->nullable()->after('os_name');
            $table->string('os_version')->nullable()->after('os_kernel');
            $table->string('os_revision')->nullable()->after('os_version');

            // software
            $table->string('software_fastd_version')->nullable()->after('os_revision');
            $table->string('software_batman_advanced_version')->nullable()->after('software_fastd_version');
            $table->string('software_batman_advanced_compatversion')->nullable()->after('software_batman_advanced_version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->dropColumn([
                'hardware_manufacturer',
                'hardware_model',
                'hardware_version',
                'hardware_cpu',

                'os_name',
                'os_kernel',
                'os_version',
                'os_revision',

                'software_fastd_version',
                'software_batman_advanced_version',
                'software_batman_advanced_compatversion',
            ]);
        });
    }
}
