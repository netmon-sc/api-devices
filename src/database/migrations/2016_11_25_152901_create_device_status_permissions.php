<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ApiServer\Authorization\Models\Role;
use ApiServer\Authorization\Models\Permission;
use ApiServer\Configs\Models\Config;

class CreateDeviceStatusPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $adminRoleId = Config::where(
            'key',
            'serverAdminRoleId'
        )->firstOrFail()->value;
        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'index',
            'resource_id' => 'device_status',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'show',
            'resource_id' => 'device_status',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'store',
            'resource_id' => 'device_status',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'update',
            'resource_id' => 'device_status',
        ]);

        Permission::create([
            'role_id' => $adminRoleId,
            'action_id' => 'destroy',
            'resource_id' => 'device_status',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('resource_id', '=', 'device_status')->delete();
    }
}
