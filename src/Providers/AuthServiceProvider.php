<?php

namespace Netmon\Devices\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Netmon\Devices\Models\Device::class => \Netmon\Devices\Policies\DevicePolicy::class,
        \Netmon\Devices\Models\DeviceAuthorization::class => \Netmon\Devices\Policies\DeviceAuthorizationPolicy::class,
        \Netmon\Devices\Models\DeviceStatus::class => \Netmon\Devices\Policies\DeviceStatusPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
