<?php

namespace Netmon\Devices\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class RelationsServiceProvider extends ServiceProvider
{
    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        add_model_relation(
            'ApiServer\Users\Models\User',
            'ownDevices',
            function($model) {
                return $model->hasMany('Netmon\Devices\Models\Device', 'owner_id');
            }
        );

        add_model_relation(
            'ApiServer\Users\Models\User',
            'createdDevices',
            function($model) {
                return $model->hasMany('Netmon\Devices\Models\Device', 'creator_id');
            }
        );

        add_serializer_relation(
            'ApiServer\Users\Serializers\UserSerializer',
            'ownDevices',
            function($serializer, $model) {
                return $serializer->hasMany($model, 'Netmon\Devices\Serializers\DeviceSerializer');
            }
        );

        add_serializer_relation(
            'ApiServer\Users\Serializers\UserSerializer',
            'createdDevices',
            function($serializer, $model) {
                return $serializer->hasMany($model, 'Netmon\Devices\Serializers\DeviceSerializer');
            }
        );
    }
}
