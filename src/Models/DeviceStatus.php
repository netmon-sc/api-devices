<?php

namespace Netmon\Devices\Models;

use ApiServer\Base\Models\BaseModel;
use ApiServer\Base\Traits\UuidForKeyTrait;

/**
 * Netmon\Server\Models\NetworkDevice
 *
 * @property integer $id
 * @property string $hostname
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereHostname($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereUpdatedAt($value)
 */
class DeviceStatus extends BaseModel
{
    use UuidForKeyTrait;

    /**
     * Bootstrap any application services.
     */
    public static function boot()
    {
        parent::boot();

        //Register validation service
        //on saving event
        self::saving(
            function ($model) {
                return $model->validate();
            }
        );

        self::updating(
            function ($model) {
                //force updated_at timestamp update for DeviceStatus
                $model->updated_at = $model->freshTimestampString();
            }
        );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'device_statuses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // meta
		'device_id',

        // general
		'local_time', 'uptime', 'idletime', 'clients',

        // resources - load
        'resources_load_1min', 'resources_load_5min', 'resources_load_15min',

        // resources - memory
        'resources_memory_total', 'resources_memory_free',
        'resources_memory_buffered', 'resources_memory_cache',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
        // These attributes are inspired by the DeviceMonitoring object of
        // the NetJSON specification http://netjson.org/rfc.html#rfc.section.5

        // meta
        'device_id' => 'required|exists:devices,id',

        //general
        'local_time' => 'nullable|integer',
        'uptime' => 'nullable|integer',
        'idletime' => 'nullable|integer',
        'clients' => 'nullable|integer',

		//resources load
		'resources_load_1min' => 'nullable|numeric',
		'resources_load_5min' => 'nullable|numeric',
		'resources_load_15min' => 'nullable|numeric',

		//resources memory
		'resources_memory_total' => 'nullable|integer',
		'resources_memory_free' => 'nullable|integer',
		'resources_memory_buffered' => 'nullable|integer',
		'resources_memory_cache' => 'nullable|integer',
    ];

    /**
     * n:1 relation to user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device() {
    	return $this->belongsTo(Device::class);
    }
}
