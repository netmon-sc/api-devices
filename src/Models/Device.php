<?php

namespace Netmon\Devices\Models;

use ApiServer\Base\Models\BaseModel;
use ApiServer\Base\Traits\UuidForKeyTrait;

/**
 * Netmon\Server\Models\NetworkDevice
 *
 * @property integer $id
 * @property string $hostname
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereHostname($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereUpdatedAt($value)
 */
class Device extends BaseModel
{
    use UuidForKeyTrait;

    /**
     * Bootstrap any application services.
     */
    public static function boot()
    {
        parent::boot();

        //Register validation service
        //on saving event
        self::saving(
            function ($model) {
                //validate
                return $model->validate();
            }
        );

        self::creating(
            function ($model) {
                // assign currently authenticated user as creator
                $user = \Auth::user();
                if(!empty($user->id)) {
                    $model->creator_id = $user->id;
                }
            }
        );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    		'owner_id', 'creator_id',
            'coordinate_id',

    		'hostname', 'description',

            'hardware_manufacturer', 'hardware_model',
            'hardware_version', 'hardware_cpu',

            'os_name', 'os_kernel', 'os_version', 'os_revision',

            'software_fastd_version',
            'software_batman_advanced_version',
            'software_batman_advanced_compatversion',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
        // These attributes are inspired by the DeviceConfiguration object of
        // the NetJSON specification http://netjson.org/rfc.html#rfc.section.5

        // meta
        'owner_id' => 'nullable|exists:users,id',
        'creator_id' => 'nullable|exists:users,id',
        'coordinate_id' => 'nullable|exists:coordinates,id',

        // general
    	'hostname' => [
    		'required',
    		'between:1,255',
            'regex:/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/'
    	],
    	'description' => '',

        // hardware
        'hardware_manufacturer' => 'nullable|string',
        'hardware_model' => 'nullable|string',
        'hardware_version' => 'nullable|string',
        'hardware_cpu' => 'nullable|string',

        // operating system
        'os_name' => 'nullable|string',
        'os_kernel' => 'nullable|string',
        'os_version' => 'nullable|string',
        'os_revision' => 'nullable|string',

        // software
        'software_fastd_version' => 'nullable|string',
        'software_batman_advanced_version' => 'nullable|string',
        'software_batman_advanced_compatversion' => 'nullable|string',
    ];

    /**
     * n:1 relation to users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner() {
    	return $this->belongsTo(\ApiServer\Users\Models\User::class);
    }

    /**
     * n:1 relation to users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator() {
    	return $this->belongsTo(\ApiServer\Users\Models\User::class);
    }

    /**
     * n:1 relation to users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coordinate() {
    	return $this->belongsTo(\Netmon\Places\Models\Coordinate::class);
    }

    /**
     * 1:n relation to device_states
     */
    public function deviceStatus()
    {
    	return $this->hasOne(DeviceStatus::class);
    }

    /**
     * 1:n relation to device_states
     */
    public function deviceAuthorizations()
    {
    	return $this->hasMany(DeviceAuthorization::class);
    }
}
