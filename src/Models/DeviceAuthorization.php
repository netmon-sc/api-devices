<?php

namespace Netmon\Devices\Models;

use ApiServer\Base\Models\BaseModel;
use ApiServer\Base\Traits\UuidForKeyTrait;
use Validator;
use Hash;

/**
 * Netmon\Server\Models\NetworkDevice
 *
 * @property integer $id
 * @property string $hostname
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereHostname($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Netmon\Server\App\Models\NetworkDevice whereUpdatedAt($value)
 */
class DeviceAuthorization extends BaseModel
{
    use UuidForKeyTrait;

    /**
     * Bootstrap any application services.
     */
    public static function boot()
    {
        parent::boot();

        //Register validation service
        //on saving event
        self::saving(
            function ($model) {
                return $model->validate();
            }
        );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'device_authorizations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    		'device_id',
    		'password',
            'type',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
    		//meta
    		'device_id' => 'required|exists:devices,id',
            'type' => 'required|in:ownership|unique_with:device_authorizations,device_id',
    ];

    /**
     * Set the password and validate before setting
     * because on store() the password will already
     * be hashed.
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        // passwords hashed with bcrypt should have at least 8 and at
        // most 56 bytes in length, http://en.wikipedia.org/wiki/Bcrypt
        $validation = Validator::make(
            array('password' => $password),
            array('password' => 'required|min:8|max:56')
        );

        if ($validation->fails()) {
            $this->passwordValidationErrors = $validation->errors();
        }
        else {
            $this->attributes['password'] = Hash::make($password);
        }
    }

    /**
     * Validate the model before storing to database.
     * This method also handles password validation
     * @return boolean
     */
    public function isValid()
    {
        $validation = Validator::make(
            $this->attributes,
            array_map(
                //replace placeholders with their real value
                function ($r) {
                    return str_replace('{id}', $this->id ?: '', $r);
                },
                $this->validationRules
            )
        );

        //check if model validation or password validation fails
        if ($validation->fails() || $this->passwordValidationErrors) {
            $this->validationErrors = $validation->errors();
            if ($this->passwordValidationErrors) {
                $this->validationErrors->merge($this->passwordValidationErrors);
            }
            return false;
        }

        return true;
    }

    /**
     * n:1 relation to users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device() {
    	return $this->belongsTo(Device::class);
    }
}
