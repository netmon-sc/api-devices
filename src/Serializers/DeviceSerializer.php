<?php

namespace Netmon\Devices\Serializers;

use Gate;

use ApiServer\JsonApi\Serializers\BaseSerializer;

use Netmon\Devices\Models\Device;

class DeviceSerializer extends BaseSerializer
{
    protected $type = 'devices';

    public function getAttributes($model, array $fields = null)
    {
        if (! ($model instanceof Device)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.Device::class
            );
        }

        // set up attributes
        $attributes = $model->getAttributes();
        $attributes['created_at']  = $this->formatDate($model->created_at);
        $attributes['updated_at'] = $this->formatDate($model->updated_at);

        return $attributes;
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/devices/{$model->id}",
        ];

        //links to include based permissions
        if(Gate::allows('show', $model))
            $links['read'] = config('app.url')."/devices/{$model->id}";
        if(Gate::allows('update', $model))
            $links['update'] = config('app.url')."/devices/{$model->id}";
        if(Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/devices/{$model->id}";

        return $links;
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function owner($model)
    {
        return $this->hasOne($model, \ApiServer\Users\Serializers\UserSerializer::class);
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function creator($model)
    {
        return $this->hasOne($model, \ApiServer\Users\Serializers\UserSerializer::class);
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function deviceStatus($model)
    {
        return $this->hasOne($model, DeviceStatusSerializer::class);
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function coordinate($model)
    {
        return $this->hasOne($model, \Netmon\Places\Serializers\CoordinateSerializer::class);
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function deviceAuthorizations($model)
    {
        return $this->hasMany($model, DeviceAuthorizationSerializer::class);
    }
}

?>
