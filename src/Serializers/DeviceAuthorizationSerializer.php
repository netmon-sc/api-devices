<?php

namespace Netmon\Devices\Serializers;

use Gate;

use ApiServer\JsonApi\Serializers\BaseSerializer;

use Netmon\Devices\Models\DeviceAuthorization;

class DeviceAuthorizationSerializer extends BaseSerializer
{
    protected $type = 'deviceAuthorizations';

    public function getAttributes($model, array $fields = null)
    {
        if (! ($model instanceof DeviceAuthorization)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.DeviceAuthorization::class
            );
        }

        return [
            'device_id' => $model->device_id,
            'type' => $model->type,
            'created_at'  => $this->formatDate($model->created_at),
            'updated_at' => $this->formatDate($model->updated_at)
        ];
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/device_authorizations/{$model->id}",
        ];

        //links to include based permissions
        if(Gate::allows('show', $model))
            $links['read'] = config('app.url')."/device_authorizations/{$model->id}";
        if(Gate::allows('update', $model))
            $links['update'] = config('app.url')."/device_authorizations/{$model->id}";
        if(Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/device_authorizations/{$model->id}";

        return $links;
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function device($model)
    {
        return $this->hasOne($model, DeviceSerializer::class);
    }
}

?>
