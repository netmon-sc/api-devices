<?php

namespace Netmon\Devices\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use ApiServer\Authorization\Policies\BasePolicy;
use Netmon\Devices\Models\Device;
use ApiServer\Users\Models\User;

class DevicePolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function index(User $authUser) {
        //everyone is allowed to list devices
        return true;
    }

    public function store(User $authUser, Device $device = null) {
        return $this->checkPermissions($authUser, 'store', 'device');
    }

    public function show(User $authUser, Device $device) {
        //everyone is allowed to show device
        return true;
    }

    public function update(User $authUser, Device $device) {
        if( !empty($authUser->id)
            && (
                $authUser->id == $device->owner_id
                || $authUser->id == $device->creator_id
            )
        )
            return true;

        return $this->checkPermissions($authUser, 'update', 'device', $device);
    }

    public function destroy(User $authUser, Device $device) {
        if( !empty($authUser->id)
            && (
                $authUser->id == $device->owner_id
                || $authUser->id == $device->creator_id
            )
        )
            return true;

        return $this->checkPermissions($authUser, 'destroy', 'device', $device);
    }
}
