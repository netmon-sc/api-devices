<?php

namespace Netmon\Devices\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use ApiServer\Authorization\Policies\BasePolicy;
use Netmon\Devices\Models\DeviceStatus;
use Netmon\Devices\Models\Device;
use ApiServer\Users\Models\User;
use Gate;

class DeviceStatusPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function index(User $authUser) {
        return true;
    }

    public function store(User $authUser, DeviceStatus $deviceStatus=null) {
        //if authenticated user is allowed to update the coresponding
        //device, then he is also allowed to store a new DeviceStatus
        try {
            if(!is_null($deviceStatus)) {
                $device = Device::findOrFail($deviceStatus->device_id);
                if(Gate::allows('update', $device)) {
                    return true;
                }
            }
        } catch(\Exception $e) {}

        //if user is not allowed to update coresponding device or
        //device does not exist, then return default permission check
        return $this->checkPermissions($authUser, 'store', 'device_status');
    }

    public function show(User $authUser, DeviceStatus $deviceStatus) {
        return true;
    }

    public function update(User $authUser, DeviceStatus $deviceStatus) {
        //if authenticated user is allowed to update the coresponding
        //device, then he is also allowed to store a new DeviceStatus
        try {
            if(!is_null($deviceStatus)) {
                $device = Device::findOrFail($deviceStatus->device_id);
                if(Gate::allows('update', $device)) {
                    return true;
                }
            }
        } catch(\Exception $e) {}

        //if user is not allowed to update coresponding device or
        //device does not exist, then return default permission check
        return $this->checkPermissions($authUser, 'update', 'device_status');
    }

    public function destroy(User $authUser, DeviceStatus $deviceStatus) {
        return $this->checkPermissions($authUser, 'destroy', 'device_status', $deviceStatus);
    }
}
