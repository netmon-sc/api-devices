<?php

namespace Netmon\Devices\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use ApiServer\Authorization\Policies\BasePolicy;
use Netmon\Devices\Models\DeviceAuthorization;
use Netmon\Devices\Models\Device;
use ApiServer\Users\Models\User;
use Gate;

class DeviceAuthorizationPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function index(User $authUser) {
        return true;
    }

    public function store(User $authUser, DeviceAuthorization $deviceAuthorization=null) {
        //if authenticated user is allowed to update the coresponding
        //device, then he is also allowed to store a new DeviceAuthorization
        try {
            if(!is_null($deviceAuthorization)) {
                $device = Device::findOrFail($deviceAuthorization->device_id);
                if(Gate::allows('update', $device)) {
                    return true;
                }
            }
        } catch(\Exception $e) {}

        //if user is not allowed to update coresponding device or
        //device does not exist, then return default permission check
        return $this->checkPermissions($authUser, 'store', 'device_authorization');
    }

    public function show(User $authUser, DeviceAuthorization $deviceAuthorization) {
        return true;
    }

    public function update(User $authUser, DeviceAuthorization $deviceAuthorization) {
        return $this->checkPermissions($authUser, 'update', 'device_authorization', $deviceAuthorization);
    }

    public function destroy(User $authUser, DeviceAuthorization $deviceAuthorization) {
        return $this->checkPermissions($authUser, 'destroy', 'device_authorization', $deviceAuthorization);
    }
}
