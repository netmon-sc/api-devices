<?php

Route::group([
    'namespace' => '\Netmon\Devices\Http\Controllers',
    'middleware' => 'cors'], function()
{
	/**
	 * Routes using JWT auth
	 */
	Route::group(['middleware' => ['auth.jwt']], function () {
        Route::get('devices/{deviceId}/status', 'DeviceNestsDeviceStatusController@show');
        Route::patch('devices/{deviceId}/status', 'DeviceNestsDeviceStatusController@update');
        Route::delete('devices/{deviceId}/status', 'DeviceNestsDeviceStatusController@delete');

        Route::post('devices/{deviceId}/become-owner', 'DevicesController@becomeOwner');
        Route::resource('devices', DevicesController::class, ['only' => [
			    'index', 'show', 'store', 'update', 'destroy'
		]]);

        Route::resource('device-authorizations', DeviceAuthorizationsController::class, ['only' => [
			    'index', 'show', 'store', 'update', 'destroy'
		]]);

        Route::resource('device-statuses', DeviceStatusesController::class, ['only' => [
			    'index', 'show', 'store', 'update', 'destroy'
		]]);
	});
});

?>
