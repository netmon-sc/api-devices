<?php

namespace Netmon\Devices\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

class DeviceStatusesController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Devices\Models\DeviceStatus::class;
    }

    public function serializer() {
        return \Netmon\Devices\Serializers\DeviceStatusSerializer::class;
    }

    public function resource() {
        return "device-statuses";
    }
}
