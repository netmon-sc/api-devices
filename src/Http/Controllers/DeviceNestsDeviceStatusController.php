<?php

namespace Netmon\Devices\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\OneToOneResourceController;

class DeviceNestsDeviceStatusController extends OneToOneResourceController
{
    public function model() {
      return \Netmon\Devices\Models\DeviceStatus::class;
    }

    public function siblingModel() {
      return \Netmon\Devices\Models\Device::class;
    }

    public function serializer() {
        return \Netmon\Devices\Serializers\DeviceStatusSerializer::class;
    }

    public function resource() {
        return "device-statuses";
    }
}
