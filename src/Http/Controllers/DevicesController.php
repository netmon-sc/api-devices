<?php

namespace Netmon\Devices\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Netmon\Server\App\Exceptions\Exceptions\UnauthorizedException;

use Illuminate\Http\Request;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;
use Netmon\Devices\Models\DeviceAuthorization;
use Netmon\Devices\Models\Device;

class DevicesController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Devices\Models\Device::class;
    }

    public function serializer() {
        return \Netmon\Devices\Serializers\DeviceSerializer::class;
    }

    public function resource() {
        return "devices";
    }

    public function becomeOwner(Request $request, $deviceId) {
        $user = \Auth::user();
        if (empty($user->id)) {
            throw new UnauthorizedException(trans('api.unauthenticated'));
        }
        $password = $request->input('password');
        
        // if someone tries to takeover ownership of a device,
        // check if the credentials are ok
        $deviceAuthorization = DeviceAuthorization
                                ::where('type', '=', 'ownership')
                                ->where('device_id', '=', $deviceId)
                                ->firstOrFail();
        if(\Hash::check($password, $deviceAuthorization->password)) {
            //if credentials are ok, change ownership and return 204
            $device = Device::findOrFail($deviceId);
            $device->owner_id = $user->id;
            $device->save();
            $deviceAuthorization->delete();
            return response()->noContent();
        } else {
            throw new AccessDeniedHttpException(
                trans('api.unauthorized_resource_store', [
                    'action' => trans('terms.store'),
                    'modelName' => trans_choice('terms.device_authorization', 1),
                ])
            );
        }
    }
}
