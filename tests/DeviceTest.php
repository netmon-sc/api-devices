<?php

namespace Netmon\Devices\Tests;

use Netmon\Server\Tests\ModuleTestCase;
use Netmon\Server\Tests\UserTest;

use Netmon\Devices\Models\Device;

class DeviceTest extends ModuleTestCase
{
    protected function moduleServiceProvider() {
        return \Netmon\Devices\ModuleServiceProvider::class;
    }

    protected function moduleServiceProviderDepencies() {
        return [
            \Netmon\Places\ModuleServiceProvider::class,
        ];
    }

	public function getStructure() {
		return [
			'type',
			'id',
			'attributes' => [
				'hostname',
                'creator_id',
                'owner_id'
			]
		];
	}

	public static function createDevice($attributes = []) {
		$deviceCount = (Device::get()->count())+1;

        if(!isset($attributes['hostname']))
            $attributes['hostname'] = "device{$deviceCount}";

		return Device::create($attributes);
	}

	public static function createDevices($howMany) {
		$devices = [];
		for($i = 0; $i < $howMany; $i++) {
			$devices[] = static::createDevice();
		}
		return $devices;
	}

    public function testDeviceCreate() {
        $user = UserTest::createUser();
		$device = DeviceTest::createDevice();
    }
}
